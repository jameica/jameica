<form>
	<p>
		<span color="header" font="header">Backups verwalten.</span>
	</p>

	<li>
		Aktivieren Sie die Option <b>Backups automatisch beim Beenden von Jameica erstellen</b>
		wenn Jameica jedes Mal beim Beenden der Anwendung ein Backup des aktuellen Benutzerverzeichnisses
		erstellen soll.
	</li>
	<li>
	  Wählen Sie zum Wiederherstellen das gewünschte Backup aus der Liste der
	  verfügbaren Backups aus und klicken Sie auf <b>Ausgewähltes Backup wiederherstellen...</b>.
	  Klicken Sie in der folgenden Sicherheitsabfrage auf <b>Ja, Backup wiederherstellen</b>,
	  um Jameica zu beenden. Die Daten werden beim nächsten Start der Anwendung wiederhergestellt.
	  <br/>
	  Klicken Sie andernfalls auf <b>Nein, Vorgang abbrechen</b>.
	</li>
</form>
